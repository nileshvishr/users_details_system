<?php

    include('../database/config/app.php');
    include('../presenter/UserController.php');

    if(isset($_POST['update_btn']))
    {
        $user_id = validateInput($db->conn,$_POST['user_id']);
        $fname = validateInput($db->conn,$_POST['fname']);
        $lname = validateInput($db->conn,$_POST['lname']);
        $email =validateInput($db->conn, $_POST['email']);

        $user = new UserController;
        $result = $user->update($user_id,$fname,$lname,$email);
        if($result)
        {
            redirect("User updated successfully", "admin/");
        }
        else
        {
            redirect("Something went wrong", "admin/");
        }
    }

    if(isset($_POST['del_user']))
    {
        $id = validateInput($db->conn,$_POST['del_user']);
        $user = new UserController;
        $result = $user->delete($id);
        if($result)
        {
            redirect("User deleted successfully", "admin/");
        }
        else
        {
            redirect("Something went wrong", "admin/");
        }
    }

    if(isset($_POST['approval_btn']))
    {
        $id = validateInput($db->conn,$_POST['user_id']);
        $approval = validateInput($db->conn,$_POST['approval']);

        $userApproved = new UserController;
        $result = $userApproved->userApproved($id,$approval);
        if($result)
        {
            redirect("User approved successfully", "admin/approval.php");
        }
        else
        {
            redirect("Something went wrong", "admin/approval.php");
        }
    }

?>