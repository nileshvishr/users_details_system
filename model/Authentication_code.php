<?php 

include_once('../../presenter/RegisterController.php');
include_once('../../presenter/LoginController.php');


$auth = new LoginController;

// for admin login

if(isset($_POST['admin_login_btn']))
{
    $email = validateInput($db->conn,$_POST['email']);
    $password = validateInput($db->conn,$_POST['password']);

    $checkLogin = $auth->adminLogin($email,$password);
    if($checkLogin)
    {
        redirect("Logged in successfully", "admin/");
    }
    else
    {
        redirect("Invalid email or password", "admin/login.php");
    }
}


// for user login

if(isset($_POST['login_btn']))
{
    $email = validateInput($db->conn,$_POST['email']);
    $password = validateInput($db->conn,$_POST['password']);
    $approval = 'approved';

    $checkApproval = $auth->userApproved($email,$password,$approval);
    if($checkApproval)
    {
        $checkLogin = $auth->userLogin($email,$password);
        if($checkLogin)
        {
            redirect("Logged in successfully", "user/");
        }
    }
    else
    {
        redirect("Wait for admin to approved your account", "user/login.php");
    }
}


// for user registration

if(isset($_POST['reg_btn']))
{
    $fname = validateInput($db->conn,$_POST['fname']);
    $lname = validateInput($db->conn,$_POST['lname']);
    $email =validateInput($db->conn, $_POST['email']);
    $password = validateInput($db->conn,$_POST['password']);
    $c_password = validateInput($db->conn,$_POST['c_password']);

    $register = new RegisterController;

    $reult_password = $register->confirmPassword($password,$c_password);
    if($reult_password)
    {
        $result_userexist = $register->isUserExist($email);
        if($result_userexist)
        {
            redirect("Already Email Exist", "user/register.php");
        }
        else
        {
            $register_query = $register->registration($fname,$lname,$email,$password);
            if($register_query)
            {
                redirect("Registered Successfully", "user/login.php");
            }
            else
            {
                redirect("Something went wrong", "user/register.php");
            }
        }
    }
    else
    {
        redirect("Password and Confirm Password Does not match", "user/register.php");
    }
}

// for user logout

if(isset($_POST['logout_btn']))
{
    $checkLoggedOut = $auth->logout();
    if($checkLoggedOut)
    {
        redirect("Logged Out Successfully", "user/login.php");
    }
}

// for admin logout

if(isset($_POST['admin_logout_btn']))
{
    $checkLoggedOut = $auth->adminlogout();
    if($checkLoggedOut)
    {
        redirect("Logged Out Successfully", "admin/login.php");
    }
}

?>