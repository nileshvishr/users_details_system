<?php
    include('database/config/app.php');
    include('view/includes/home_header.php');
?>

<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="box">
                <div class="admin-card">
                    <h4>Use this button for admin login</h4>
                    <span>|</span><br>
                    <span>|</span><br>
                    <span>V</span>
                    <div class="form-group mt-4">
                        <a href="view/admin/login.php" class="btn btn-success">Admin Login</a>
                    </div>
                </div>
                <div class="user-card">
                    <h4>Use this button for user login</h4>
                    <span>|</span><br>
                    <span>|</span><br>
                    <span>V</span>
                    <div class="form-group mt-4">
                        <a href="view/user/login.php" class="btn btn-primary">User Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    include('view/includes/home_footer.php');
?>