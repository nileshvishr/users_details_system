<?php

class AdminController
{
    public function __construct()
    {
        $db = new DatabaseConnection;
        $this->conn = $db->conn;
        $this->isLoggedOut();
    }

    private function isLoggedOut()
    {
        if(!isset($_SESSION['authenticated']))
        {
            redirect("You need to logged in first", "admin/login.php");
            return false;
        }
        else
        {
            return true;
        }
    }

    public function userDetails()
    {
        $approval = 'approved';
        $userQuery = "SELECT * FROM users WHERE approval='$approval'";
        $result = $this->conn->query($userQuery);
        if($result->num_rows > 0)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    public function needApproval()
    {
        $approval = 'in_process';
        $userQuery = "SELECT * FROM users WHERE approval='$approval'";
        $result = $this->conn->query($userQuery);
        if($result->num_rows > 0)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    public function edit($id)
    {
        $user_id = validateInput($this->conn, $id);
        $userQuery = "SELECT * FROM users WHERE id='$user_id' LIMIT 1";
        $result = $this->conn->query($userQuery);
        if($result->num_rows == 1)
        {
            $data = $result->fetch_assoc();
            return $data;
        }
        else
        {
            return false;
        }
    }
}

$userData = new AdminController;


?>