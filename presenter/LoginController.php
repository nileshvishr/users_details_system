<?php

class LoginController
{
    public function __construct()
    {
        $db = new DatabaseConnection;
        $this->conn = $db->conn;
    }

    // for admin auth

    public function adminLogin($email,$password)
    {
        $checkLogin = "SELECT * FROM admin WHERE email='$email' AND password='$password' LIMIT 1";
        $result = $this->conn->query($checkLogin);
        if($result->num_rows > 0)
        {
            $data = $result->fetch_assoc();
            $this->userAuthentication($data);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function adminLoggedIn()
    {
        if(isset($_SESSION['authenticated']) === TRUE)
        {
            redirect("You are already logged in", "admin/");
        }
        else
        {
            return false;
        }
    }

    // for user auth

    public function userLogin($email,$password)
    {
        $checkLogin = "SELECT * FROM users WHERE email='$email' AND password='$password' LIMIT 1";
        $result = $this->conn->query($checkLogin);
        if($result->num_rows > 0)
        {
            $data = $result->fetch_assoc();
            $this->userAuthentication($data);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function userApproved($email,$password,$approval)
    {
        $checkLoginfirst = "SELECT * FROM users WHERE email='$email' AND password='$password' LIMIT 1";
        $result_login = $this->conn->query($checkLoginfirst);
        if($result_login->num_rows > 0)
        {   
            $checkApproval = "SELECT * FROM users WHERE email='$email' AND password='$password' AND approval='$approval' LIMIT 1";
            $result = $this->conn->query($checkApproval);
            if($result->num_rows > 0)
            {
                $data = $result->fetch_assoc();
                $this->userAuthentication($data);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            redirect("Invalid email or password", "user/login.php");
        }
    }
    
    public function isLoggedIn()
    {
        if(isset($_SESSION['authenticated']) === TRUE)
        {
            redirect("You are already logged in", "user/");
        }
        else
        {
            return false;
        }
    }

    // common for both adim and user

    private function userAuthentication($data)
    {
        $_SESSION['authenticated'] = true;
        $_SESSION['auth_user'] = [
            'user_id' => $data['id'],
            'user_fname' => $data['fname'],
            'user_lname' => $data['lname'],
            'user_email' => $data['email']
        ];
    }

    public function logout()
    {
        if(isset($_SESSION['authenticated']) === TRUE)
        {
            unset($_SESSION['authenticated']);
            unset($_SESSION['auth_user']);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function adminlogout()
    {
        if(isset($_SESSION['authenticated']) === TRUE)
        {
            unset($_SESSION['authenticated']);
            unset($_SESSION['auth_user']);
            return true;
        }
        else
        {
            return false;
        }
    }
}


?>