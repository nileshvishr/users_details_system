<?php

class UserController
{
    public function __construct()
    {
        $db = new DatabaseConnection;
        $this->conn = $db->conn;
        $this->isLoggedOut();
    }

    private function isLoggedOut()
    {
        if(!isset($_SESSION['authenticated']))
        {
            redirect("You need to logged in first", "user/login.php");
            return false;
        }
        else
        {
            return true;
        }
    }

    public function userDetails()
    {
        $approval = 'approved';
        $userQuery = "SELECT * FROM users WHERE approval='$approval'";
        $result = $this->conn->query($userQuery);
        if($result->num_rows > 0)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    public function update($user_id,$fname,$lname,$email)
    {
        $id = validateInput($this->conn, $user_id);
        $userUpdateQuery = "UPDATE users SET fname='$fname', lname='$lname', email='$email' WHERE id='$id' LIMIT 1";
        $result = $this->conn->query($userUpdateQuery);
        if($result)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function delete($id)
    {
        $user_id = validateInput($this->conn, $id);
        $userDeleteQuery = "DELETE FROM users WHERE id='$user_id' LIMIT 1";
        $result = $this->conn->query($userDeleteQuery);
        if($result)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function userApproved($id,$approval)
    {
        $user_id = validateInput($this->conn, $id);
        $userApprovalQuery = "UPDATE users SET approval='$approval' WHERE id='$id' LIMIT 1";
        $result = $this->conn->query($userApprovalQuery);
        if($result)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

$userData = new UserController;


?>