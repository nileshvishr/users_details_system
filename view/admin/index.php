<?php
    include('../../database/config/app.php');
    include('../../model/Authentication_code.php');
    include_once('../../presenter/AdminController.php');
    require_once('../includes/header.php');
    require_once('../includes/admin_nav.php');
?>

<div class="container mt-4">
    <?php include('../includes/message.php'); ?>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th class="text-center">Approval</th>
                <th colspan="2" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $result = $userData->userDetails();
                if($result)
                {
                    foreach($result as $user)
                    {
                        ?>
                            <tr>
                                <td>1</td>
                                <td><?= $user['fname'] ?></td>
                                <td><?= $user['lname'] ?></td>
                                <td><?= $user['email'] ?></td>
                                <td class="text-center"><?= $user['approval'] ?>
                                    <!-- <form action="" method="POST">
                                        <input type="hidden" name="approval" value="approved">
                                        <input type="hidden" name="approval" value="<?= $user['id'] ?>">
                                        <button type="submit" name="approval_btn" class="btn btn-success">Approved</button>
                                    </form> -->
                                </td>
                                <td class="text-center">
                                    <a href="edit.php?id=<?= $user['id'] ?>" class="btn btn-success">Edit</a>
                                </td>
                                <td class="text-center">
                                    <form action="../../model/User_code.php" method="POST">
                                        <button type="submit" name="del_user" value="<?= $user['id'] ?>" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                    }
                }
                else
                {
                    echo 'No record found';
                }
            ?>
        </tbody>
    </table>
</div>

<?php
    require_once('../includes/footer.php');
?>