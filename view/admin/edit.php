<?php
    include('../../database/config/app.php');
    include('../../model/Authentication_code.php');
    include_once('../../presenter/AdminController.php');
    require_once('../includes/header.php');
    require_once('../includes/admin_nav.php');
?>

<div class="container">
    <div class="card">
        <?php include('../includes/message.php'); ?>
        <div class="card-header">
            <h1>Edit User</h1>
        </div>
        <div class="card-body">
            <?php
                if(isset($_GET['id']))
                {
                    $user_id = validateInput($db->conn,$_GET['id']);
                    $result = $userData->edit($user_id);
                    if($result)
                    {
                        ?>
                        <form action="../../model/User_code.php" method="post">
                            <div class="form-group">
                                <input type="hidden" name="user_id" value="<?= $result['id'] ?>">
                                <input type="text" name="fname" class="form-control" value="<?= $result['fname'] ?>" />
                            </div>
                            <div class="form-group">
                                <input type="text" name="lname" class="form-control" value="<?= $result['lname'] ?>" />
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" value="<?= $result['email'] ?>" />
                            </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" name="update_btn" class="btn btn-primary">Update</button>
                    </form>
                    <?php
                    }
                    else
                    {
                        echo "<h4>No Record Found</h4>";
                    }
                }
            ?>
        </div>
    </div>
</div>

<?php
    include('../includes/footer.php');
?>