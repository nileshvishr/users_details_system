<?php
    include('../../database/config/app.php');
    include('../../model/Authentication_code.php');
    include_once('../../presenter/AdminController.php');
    require_once('../includes/header.php');
    require_once('../includes/admin_nav.php');
?>

<div class="container mt-4">
    <?php include('../includes/message.php'); ?>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th class="text-center">Approval</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $result = $userData->needApproval();
                if($result)
                {
                    foreach($result as $user)
                    {
                        ?>
                            <tr>
                                <td>1</td>
                                <td><?= $user['fname'] ?></td>
                                <td><?= $user['lname'] ?></td>
                                <td><?= $user['email'] ?></td>
                                <td class="text-center">
                                    <form action="../../model/User_code.php" method="POST">
                                        <input type="hidden" name="approval" value="approved">
                                        <input type="hidden" name="user_id" value="<?= $user['id'] ?>">
                                        <button type="submit" name="approval_btn" class="btn btn-success">Need Approval</button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                    }
                }
                else
                {
                    echo '<tr><td colspan="5" class="text-center">No user found to approved</td></tr>';
                }
            ?>
        </tbody>
    </table>
</div>

<?php
    require_once('../includes/footer.php');
?>