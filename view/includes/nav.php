<header>
    <nav class="container">
        <div class="left">
            <h1><a href="./">UDS</a></h1>
        </div>
        <div class="right">
            <ul>
                <?php if(isset($_SESSION['authenticated'])) : ?>
                <li><?= $_SESSION['auth_user']['user_fname']." ".$_SESSION['auth_user']['user_lname'] ?></li>
                <?php endif; ?>
                <li><a href="./">User Listing</a></li>
                <li>
                    <form action="" method="POST">
                        <button type="submit" name="logout_btn" class="btn-logout">Logout</button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>
</header>