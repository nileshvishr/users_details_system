<?php
    include('../../database/config/app.php');
    include('../../model/Authentication_code.php');
    include_once('../../presenter/UserController.php');

    include('../includes/header.php');
    include('../includes/nav.php');
?>

<div class="container mt-4">
    <?php include('../includes/message.php'); ?>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $result = $userData->userDetails();
                if($result)
                {
                    foreach($result as $user)
                    {
                        ?>
                            <tr>
                                <td>1</td>
                                <td><?= $user['fname'] ?></td>
                                <td><?= $user['lname'] ?></td>
                                <td><?= $user['email'] ?></td>
                            </tr>
                        <?php
                    }
                }
                else
                {
                    echo 'No record found';
                }
            ?>
        </tbody>
    </table>
</div>

<?php
    include('../includes/footer.php');
?>