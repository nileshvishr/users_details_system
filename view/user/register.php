<?php
    include('../../database/config/app.php');
    include('../../model/Authentication_code.php');
    $auth->isLoggedIn();
    include('../includes/header.php');
?>

<div class="container">
    <div class="card">
        <?php include('../includes/message.php'); ?>
        <div class="card-header">
            <h1>Register</h1>
        </div>
        <div class="card-body">
            <form action="" method="post">
                <div class="form-group">
                    <input type="text" name="fname" class="form-control" placeholder="First Name" />
                </div>
                <div class="form-group">
                    <input type="text" name="lname" class="form-control" placeholder="Last Name" />
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email" />
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" />
                </div>
                <div class="form-group">
                    <input type="password" name="c_password" class="form-control" placeholder="Confirm Password" />
                </div>
        </div>
        <div class="card-footer">
            <a href="login.php">Already have an account?</a>
            <button type="submit" name="reg_btn" class="btn btn-primary">Register</button>
        </form>
        </div>
    </div>
</div>

<?php
    include('../includes/home_footer.php');
?>